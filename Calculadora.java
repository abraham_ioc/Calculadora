public class Calculadora {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = 3;
        int suma = num1 + num2;
        int resta = num1 - num2;
        int multiplicacio = num1 * num2;
        double divisio = 0;

        try {
            divisio = dividir(num1, num2);
            System.out.println("La divisió de " + num1 + " i " + num2 + " és: " + divisio);
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println("La suma de " + num1 + " i " + num2 + " és: " + suma);
        System.out.println("La resta de " + num1 + " i " + num2 + " és: " + resta);
        System.out.println("La multiplicació de " + num1 + " i " + num2 + " és: " + multiplicacio);
    }

    public static double dividir(double a, double b) {
        if (b == 0) {
            throw new ArithmeticException("No es pot dividir per zero");
        }
        return a / b;
    }
}
